//
//  EmpresasApp.swift
//  Empresas
//
//  Created by iris on 29/11/21.
//

import SwiftUI

@main
struct EmpresasApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
