//
//  ContentView.swift
//  Empresas
//
//  Created by iris on 29/11/21.
//

import SwiftUI

struct ContentView: View {
    @State private var showLaunchView : Bool = true
    
    var body: some View {
        ZStack {
            LoginView()
            
            if(showLaunchView) {
                LaunchScreen()
            }
        }.onAppear(perform: delay)
    }
    private func delay() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.7) {
            showLaunchView.toggle()
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
