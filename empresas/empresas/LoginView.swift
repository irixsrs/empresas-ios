//
//  LoginView.swift
//  Empresas
//
//  Created by iris on 30/11/21.
//

import SwiftUI

struct LoginView: View {
   
    var body: some View {
        ZStack{
            Image("BGLogin")
                .resizable()
                .scaledToFill()
                .ignoresSafeArea()
            
            VStack{
                VStack{
                    Spacer()
                    WelcomeText()
                    
                    HStack{
                        VStack{
                            HStack{
                                Text("Digite seus dados para continuar.")
                                    .font(.subheadline)
                                    .fontWeight(.medium)
                                Spacer()
                            }.padding(.vertical)

                            Email()
                            Password()
                            Button(action: {},label: { ButtonDesign() })
                            Spacer()
                        }
                        Spacer()
                    }
                    .padding(.all)
                    .background(Color("AccentColor"))
                }
            }
        }
    }
}

struct WelcomeText: View {
    var body: some View {
        //texto boas vindas
        VStack{
            Spacer()
            HStack{
                Text("Boas Vindas,")
                    .font(.largeTitle)
                    .fontWeight(.bold)
                    .foregroundColor(Color("AccentColor"))
                Spacer()
            }
            HStack{
                Text("Você está no Empresas.")
                    .font(.title3)
                    .foregroundColor(Color("AccentColor"))
                Spacer()
            }
        }.padding([.leading, .bottom, .trailing])
    }
}

struct ButtonDesign: View {
    var body: some View {
        HStack{
            Spacer()
            Text("ENTRAR")
                .font(.subheadline)
                .fontWeight(.medium)
                .foregroundColor(Color("AccentColor"))
            Spacer()
        }
        .padding(.all)
        .background(Color.gray)
        .cornerRadius(20)
    }
}

struct Email: View {
    @State private var email: String = ""
    
    var body: some View {
        Group{
            HStack {
                Text( email != "" ? "Email" : "")
                    .font(.footnote)
                    .foregroundColor(Color.gray)
                    .padding(.bottom, -20)
                Spacer()
            }.padding(.leading, 16.0)
            
            TextField("Email", text: $email)
                .padding(.all)
                .overlay(
                    RoundedRectangle(cornerRadius: 10)
                        .stroke(Color.black, lineWidth: 0.3)
                ).padding(.top, -5)
        }.padding(.top)
    }
}

struct Password: View {
    @State private var senha: String = ""
    
    var body: some View {
        Group{
            HStack {
                Text( senha != "" ? "Senha" : "")
                    .font(.footnote)
                    .foregroundColor(Color.gray)
                    .padding(.bottom, -20)
                Spacer()
            }.padding(.leading, 16.0)
            
            TextField("Senha", text: $senha)
                .padding(.all)
                .overlay(
                    RoundedRectangle(cornerRadius: 10)
                        .stroke(Color.black, lineWidth: 0.3)
                ).padding(.top, -5)
                .padding(.bottom, 25.0)
        }
        .padding(.top)
    }
}

struct LoginView_Previews: PreviewProvider {
    static var previews: some View {
        LoginView()
        LoginView()
            .previewDevice("iPod touch (7th generation)")
    }
}
