//
//  LaunchScreen.swift
//  Empresas
//
//  Created by iris on 30/11/21.
//

import SwiftUI

struct LaunchScreen: View {
    @State private var logoMove : Bool = false
    
    var body: some View {
        ZStack{
            Image(logoMove ? "BGLaunchScreen2" : "BGLaunchScreen")
                .resizable()
                .scaledToFill()
                .ignoresSafeArea()
                .animation(Animation.easeIn(duration: 0.5))
            
                Image("LogoLaunchScreen")
                    .frame(width: 150.0, height: 57.0)
                    .offset(y: logoMove ? -100 : 0)
                    .animation(Animation.easeInOut(duration: 0.5))
            
        }.onAppear(perform: delay)
    }
    private func delay() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            logoMove.toggle()
                }
    }
}

struct LaunchScreen_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            LaunchScreen()
            LaunchScreen()
                .previewDevice("iPod touch (7th generation)")
            LaunchScreen()
                .previewDevice("iPhone 12 mini")
        }
    }
}
